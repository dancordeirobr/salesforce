@isTest
private class AccountManagerTest {
    
    static Id createTestAccount(){
        List<Contact> cons = new List<Contact>();
        Account acc = new Account(Name = 'Test Account');
        insert acc;
        for (Integer i=0;i<3;i++) {
            Contact con = new Contact();
            con.AccountId = acc.ID;
            con.LastName = 'Test Contact ' +i;
            cons.add(con);
        }
        insert cons;
        return acc.Id;
    }
    @isTest static void testGetAccount() {
        Id recordId = createTestAccount();
        RestRequest request = new RestRequest();
        request.requestUri =
            'https://brave-shark-d9d1d-dev-ed.my.salesforce.com/services/apexrest/Accounts/' + recordId + '/contacts/';
        request.httpMethod = 'GET';
        RestContext.request = request;
        Account testAcc = [SELECT Id, Name, (SELECT Id, Name FROM Contacts) FROM Account];
        Account compareAcc = AccountManager.getAccount();
        System.assert(compareAcc != null);
        System.assertEquals(testAcc, compareAcc);
    }
    
}
