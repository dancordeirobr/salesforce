public class AccountUtils {
    
    public static List<Account> accountsByState(String abbr) {
        List<Account> accs = [SELECT Id, Name FROM Account WHERE BillingState = :abbr];
        return accs;
    }
}
