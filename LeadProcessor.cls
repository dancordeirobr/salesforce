global class LeadProcessor implements Database.Batchable<SObject> {
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('SELECT Id, LeadSource FROM Lead');
    }

    global void execute(Database.BatchableContext bc, List<Lead> records) {
        List<Lead> leads = new List<Lead>();
        for (Lead eachLead : records) {
            eachLead.LeadSource = 'Dreamforce';
            leads.add(eachLead);
        }
        update leads;
    }

    global void finish(Database.BatchableContext bc) {
        
    }
}
