global class MyBatchClass implements Database.Batchable<SObject> {
    global Database.QueryLocator start(Database.BatchableContext bc) {
        // collect the batches of records or objects to be passed to execute
        String query = 'SELECT Id, Name, Big_Fucking_Deal__c FROM Account LIMIT 1000';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext bc, List<Account> records){
        // process each batch of records
        List<Account> updatedAccs = new List<Account>();
        for (Account acc :records) {
            if (acc.Name.contains('Oil')) {
                acc.Big_Fucking_Deal__c = false;
                updatedAccs.add(acc);
            }
        }
        update updatedAccs;
    }    
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
    }    
}