public with sharing class AccountProcessor {
    
    @future
    public static void countContacts(List<Id> myListOfAccountIds) {
    
        Map<Id,Integer> contactCountByAccountId = new Map<Id,Integer>();
        List<Account> updateAccounts = new List<Account>();
        List<Account> searchedAccounts = [SELECT Id, Number_of_Contacts__c, (SELECT Id FROM Contacts) 
                                                FROM Account  
                                                WHERE Id IN :myListOfAccountIds];
        for (Account acc :searchedAccounts) {
            contactCountByAccountId.put(acc.Id, acc.Contacts.size());
        }  
        for (Id accId :myListOfAccountIds) {
            if (contactCountByAccountId.containsKey(accId)) {
                Account acc = new Account();
                acc.Id = accId;
                acc.Number_of_Contacts__c = contactCountByAccountId.get(accId);
                updateAccounts.add(acc);
            }
        }  
        update updateAccounts;
    }
}
/*
001bsdhvbAKHJDVKV => 3,
001BCHJbafdhjcbhd => 2,
001hdhgygvcfdvdcq => 8,



*/