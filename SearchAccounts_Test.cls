@isTest
public with sharing class SearchAccounts_Test {
    
    public @IsTest
    static void shouldCheckSearchAccounts(){
        List<Account> myAccs = new List<Account>();
        Account acc1 = new Account(Name = 'My Oil Company');
        Account acc2 = new Account(Name = 'Oil System INC');
        Account acc3 = new Account(Name = 'International Oil');
        myAccs.add(acc1);
        myAccs.add(acc2);
        myAccs.add(acc3);
        insert myAccs;

        System.assertEquals(myAccs, SearchAccounts.searchAcc('oil'));
        System.assertEquals(3, SearchAccounts.searchAcc('oil').size());

        Test.startTest();
        
        Test.stopTest();
        
    }
    
}
