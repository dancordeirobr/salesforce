@isTest
public with sharing class DailyLeadProcessorTest {
    
    @TestSetup
    static void setup(){
        List<Lead> insertLeads = new List<Lead>();
        for (Integer i=0; i<200; i++) {
            Lead newLead = new Lead(LastName = 'Test Name ' + i, Company = 'Test Company ' + i, Status = 'Open - Not Contacted');
            insertLeads.add(newLead);
        }
        insert insertLeads;
    }

    static testmethod void testScheduledJob(){
        
        List<Lead> queriedLeads = [SELECT Id, LastName FROM Lead];
        String sch = '0 0 23 ? * 1';
        Test.startTest();
        String jobId = System.schedule('Scheduable Test', sch, new DailyLeadProcessor());
        Test.stopTest();
        List<Lead> leadsAfterTest = [SELECT Id, LastName FROM Lead WHERE LeadSource = 'Dreamforce'];
        System.assertEquals(200, leadsAfterTest.size());
    }
}
