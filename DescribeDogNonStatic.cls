public with sharing class DescribeDogNonStatic {

    private String hairType;
    private String dogSize;

    public DescribeDogNonStatic() {

    }

    public void loadHairType(String hair) {
            this.hairType = hair;
    }

    public void loadSize(String size) {
            this.dogSize = size;
    }

    public String dogDescription() {
            String description = 'Mutt';
            if(this.hairType.toLowerCase() == 'wire' && this.dogSize.toLowerCase() == 'small') {
                return 'Schnauzer';
            } else if(this.hairType.toLowerCase() == 'wire' && this.dogSize.toLowerCase() == 'toy') {
                return 'Poodle';
            } else if(this.hairType.toLowerCase() == 'short' && this.dogSize.toLowerCase() == 'large') {
                return 'Great Dane';
            }
            return description;
    }
}
