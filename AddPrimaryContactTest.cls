@isTest
public with sharing class AddPrimaryContactTest {
    
    @TestSetup
    static void setup(){
        List<Account> accounts = new List<Account>();
        for (Integer i=0;i<50;i++) {
            accounts.add(new Account(Name = 'NY Test Account ' + i, BillingState = 'NY'));
        }
        for (Integer i=0;i<50;i++) {
            accounts.add(new Account(Name = 'CA Test Account ' + i, BillingState = 'CA'));
        }
        insert accounts;
    }
    
    static testmethod void AddPrimaryContactTest() {
        Contact testcon = new Contact(LastName = 'Bond', FirstName = 'James');
        insert testcon;
        AddPrimaryContact contactTest = new AddPrimaryContact(testcon, 'CA');
        Test.startTest();
        System.enqueueJob(contactTest);
        Test.stopTest();
        System.assertEquals(50, [SELECT count() FROM Contact WHERE AccountId IN (SELECT Id FROM Account WHERE BillingState = 'CA')]);
        
    } 
}
//String sch = '0 30 * * * * *';
//String sch = '0 0 * * * * *';

/* Flex Queue
7:55 AM 
Batch Job 1

Batch Job 2
Future Job 1
Batch Job 3

*/