@isTest
public with sharing class DescribeDogNonStatic_TEST {

    public @IsTest
    static void shouldCheckDogDescription(){
        DescribeDogNonStatic myClass = new DescribeDogNonStatic();
        myClass.loadHairType('wire');
        myClass.loadSize('toy');
        DescribeDogNonStatic myClass2 = new DescribeDogNonStatic();
        myClass2.loadHairType('small');
        myClass2.loadSize('toy');
        Test.startTest();
        
        Test.stopTest();
        System.assertEquals('Mutt', myClass2.dogDescription());
        System.assertEquals('Poodle', myClass.dogDescription());
        
    }
}
