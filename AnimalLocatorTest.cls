@isTest
private class AnimalLocatorTest {
    
    @isTest static void shouldTestAnimalLocator() {
        test.setMock(HttpCalloutMock.class, new AnimalLocatorMock());
        String response = AnimalLocator.getAnimalNameById(1);
        String foo = 'chicken';
        System.assertEquals(foo, response);
    }
}
