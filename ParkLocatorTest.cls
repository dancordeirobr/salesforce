@isTest
public class ParkLocatorTest {
    @isTest static void testCallout() {
        Test.setMock(WebServiceMock.class, new ParkServiceMock());
        String country = 'United States';
        List<String> parks = new List<String>();
        parks.add('Yellowstone');
        parks.add('Mackinac National Park');
        parks.add('Yosemite');
        List<String> usParks = ParkLocator.country(country);
        System.assertEquals(parks, usParks);
    }
}
