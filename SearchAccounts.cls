public class SearchAccounts {

    public static List<Account> searchAcc(String mySearch) {
        String queryTerm = '%' + mySearch + '%';
        List<Account> myAccounts = [SELECT 
                                    	Id, 
                                    	Name,
                                    	BillingState
                                    FROM Account 
                                    WHERE Name LIKE :queryTerm];
        return myAccounts;
    }
}