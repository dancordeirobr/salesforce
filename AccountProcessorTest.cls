@isTest
public with sharing class AccountProcessorTest {
    public @IsTest
    static void shouldCountNumContacts(){
        //Test setup
        List<Account> setupListAcc = new List<Account>();
        List<Contact> setupListCon = new List<Contact>();
        List<Id> accIds = new List<Id>();
        for(Integer i=0; i<=2; i++) {
            Account acc = new Account(Name = 'Test ' + i);
            setupListAcc.add(acc);
        }
        insert setupListAcc;
        for (Account eachAcc :[SELECT Id FROM Account]) {
            accIds.add(eachAcc.Id);
            for (Integer i=0; i<=2; i++) {
                Contact con = new Contact(LastName = 'Test Name ' + i, AccountId = eachAcc.Id);
                setupListCon.add(con);
            }
        }
        insert setupListCon;

        Test.startTest();
        //Run the code
        AccountProcessor.countContacts(accIds);

        Test.stopTest();
        //Assertions
        List<Account> finishedAccounts = [SELECT Id, Number_of_Contacts__c FROM Account];
        for (Account eachAcc :finishedAccounts) {
            System.assertEquals(eachAcc.Number_of_Contacts__c, 3);
        }
    
    }
}
/*
001jhgwgfdeeyqwfg => Account Test 0
    003ywgdhvbhjvg => Last Name = Test Name 0, AccountId = 001jhgwgfdeeyqwfg
    003vsgVSGVcjas => Last Name = Test Name 1, AccountId = 001jhgwgfdeeyqwfg
    
001hjgsdhgdghgfgf => Account Test 1
    003eyuegfqgwfe => Last Name = Test Name 0, AccountId = 001hjgsdhgdghgfgf

*/