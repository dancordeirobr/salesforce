public with sharing class DailyLeadProcessor implements Schedulable {
    public void execute(SchedulableContext cxt) {
        List<Lead> leads = [SELECT Id, LeadSource FROM Lead WHERE LeadSource = null LIMIT 200];
        List<Lead> updatedLeads = new List<Lead>();
        for (Lead eachLead : leads) {
            eachLead.LeadSource = 'Dreamforce';
            updatedLeads.add(eachLead);
        }
        insert updatedLeads;
    }
}
