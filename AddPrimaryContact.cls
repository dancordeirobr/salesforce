public with sharing class AddPrimaryContact implements Queueable {

    private Contact contact;
    private String stateAbbr;

    public AddPrimaryContact(Contact contactObj, String stateAbbreviation) {
        this.contact = contactObj;
        this.stateAbbr = stateAbbreviation;
    }

    public void execute(QueueableContext context) {
        List<Account> accounts = [SELECT Id, Name, BillingState FROM Account WHERE BillingState = :stateAbbr LIMIT 200];
        // List of Contact - To be placed later
        List<Contact> contacts = new List<Contact>();

        //Loop through the accounts, clone the passed in contact, all arg. 
        //for clone method are false, set account id as lookup field on clone object.
        for (Account acc : accounts) {
            Contact con = contact.clone();
            con.AccountID = acc.Id;
            contacts.add(con);
        }
        //Add to list and insert
        insert contacts;
    }
}
